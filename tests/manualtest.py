import rawgpy

rawg = rawgpy.RAWG(
    "rawgpy testing")
results = rawg.search("")  # defaults to returning the top 5 results
game = results[0]
game.populate()  # get additional info for the game

print(game.name)

print(game.description)

for store in game.stores:
    print(store.url)

rawg.login("", "")
print(rawg.headers)
me = rawg.current_user()
print(me.name)
me.populate()
for game in me.playing:
    print(game.name)
