#
# the following is a rough idea of how im going to make the rawg API wrapper
#
# 1. i did not include every variable, since those are too numerous to describe
# 2. "methodname - -()" denotes that there should be a variable AND a method for this
# 3. im mostly going to cover the most important features


game = rawg.test_game()

game = rawg.search()  # type Game
game.populate()  # populate Game with information retrieved from rawg, this method should exist for every type of object
game.in_collections - - ()  # in which collections is this game, var if only shown, func if all
game.rate(rating)  # rate the game
game.categorize(category: Category or identifier)
game.wishlist()
game.collection(collection: Collection or identifier)
game.comment(comment body)

# all of these have name_length
game.similar - -()  # var if only shown, func if all
game.images - -() 
game.videos - -()
game.achivements - -()
game.posts - -()
game.streams - -()
game.updates - -()
game.reviews - -()  # unpopulated review objects
game.comments - -()  # unpopulated comments objects
game.screenshots - -() # has to have identifier accessible
game.creators - -()
game.stores  # list of store URLs

game.edit(
    alternative_titles=None,
    about="current",
    releas_date="current",
    esrb="current",
    metascore="current_url"
    platform=None,
    genres=None,
    publishers=None,
    developers=None,
    website="current_url"
    reddit="current_url"
)
game.edit_stores(
    add_store=None, # add store by url
    remove_store=None # remove store by name
)

game.edit_tags(
    add_tag=None, # add tag by url
    remove_tag=None # remove tag by name
)
game.add_screenshot(url)
game.replace_screenshot(url, identifier)
game.remove_screenshot(identifier)

user = rawg.user(name)  # name, @name, or user url
user.bio # biography
user.favourites # list of unpopulated Game
user.gamestats
user.reviewstats
user.collectionstats
user.developerstats
user.genrestats
user.recently_added
user.library  # dict: categoryname: list_of_unpopulated_Game
user.wishlist  # dict: list_of_unpopulated_Game
user.reviews  # alternatively, ratings
user.collections # list of collection obj (unpopulated)
user.following  # list of users this user is following
user.followers # list of followers this user has

user.logged_in()  # check if token is known
user.edit(
    full_name="current",
    username="current",
    bio="current"
    delete_avatar=False,
)

collection = rawg.collection(title)
collection.creator
collection.created_at
collection.title
collection.games - -() # list of unpopulated games
collection.edit(
    title="current",
    description="current",
    delete=False
)
collection.add(game) # or identifier/slug?
collection.upvote
collection.downvote
collection.votes
collection.recommended_collections # unpopulated Collection objects


dev = game.developer  # type Store
dev.populate()
dev.games  # list of Game object (unpopulated)
